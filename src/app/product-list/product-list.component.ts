import { Component, OnInit } from '@angular/core';
import sampleData from '../product.json';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  public productList:{
    p_name:string, 
    p_id:number,
    p_cost:number,
    p_availability:string,
    p_details:string,
    p_category:string,
    p_image:string
  }[] = sampleData;

  constructor() { }

  ngOnInit(): void {
  }



  addtoCart(){
    alert(JSON.stringify(this.productList));
  }
  

}
